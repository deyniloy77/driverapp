import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriverAvailPage } from './driver-avail';

@NgModule({
  declarations: [
    DriverAvailPage,
  ],
  imports: [
    IonicPageModule.forChild(DriverAvailPage),
  ],
})
export class DriverAvailPageModule {}
